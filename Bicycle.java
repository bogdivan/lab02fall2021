//Bogdan Ivan
//2032824
public class Bicycle {
    private String manufacturer;
    private int numberGears;
    private double maxSpeed;

    public Bicycle(String manufacturer, int numberGears, double maxSpeed) {
        this.manufacturer = manufacturer;
        this.numberGears = numberGears;
        this.maxSpeed = maxSpeed;
    }

    public String getManu(){
        return this.manufacturer;
    }

    public int getGears(){
        return this.numberGears;
    }

    public double getMaxSpeed(){
        return this.maxSpeed;
    }

    public String toString(){
        return "Manufacturer: " + this.manufacturer + ".\n" + "Number of gears: " + this.numberGears + ".\n" + "Maximum Speed: " + this.maxSpeed + ".";
    }
}