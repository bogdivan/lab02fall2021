//Bogdan Ivan
//2032824
public class BikeStore {
    public static void main(String[] args) {
        Bicycle[] bikes = new Bicycle[]{
            new Bicycle("Original Bikes", 10 , 20.0),
            new Bicycle("A Real Company", 6, 50.0),
            new Bicycle("Some Manufacturer", 15, 35.0),
            new Bicycle("AMD", 64, 3.79),
        };
        for (int i = 0; i < bikes.length; i++){
            System.out.println(bikes[i]);
            System.out.println();
        }
    }
}
